import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Subject} from 'rxjs';

@Injectable()
export class UserService {
  users: object;
  particularUser: object;
  userFetcher = new Subject<object>();
  particularFetcher = new Subject<object>();

  constructor(private http: HttpClient) {}

  fetch() {
    this.http.get<object>('https://randomuser.me/api/?results=100').subscribe((res) => {
      this.users = res['results'];
      this.userFetcher.next(this.users);
      // console.log(this.users['0']);
      // console.log(this.users['1']);
    });
  }

  getUser() {
    return this.particularUser;
  }

  fetchParticular(id: number) {
    this.http.get<object>(`https://randomuser.me/api/?id=${id}`).subscribe((res) => {
      this.particularUser = res['results'];
      this.particularFetcher.next(this.particularUser);
      // console.log(this.particularUser);
    });
  }
}
