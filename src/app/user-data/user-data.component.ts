import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {UserService} from '../user-list/user.service';

@Component({
  selector: 'app-user-data',
  templateUrl: './user-data.component.html',
  styleUrls: ['./user-data.component.css']
})
export class UserDataComponent implements OnInit, OnDestroy {
  detailedUser: object;
  id: number;

  constructor(private route: ActivatedRoute, private userService: UserService) { }

  ngOnInit() {
    this.route.params.subscribe((id: Params) => {
      this.id = +id['id'];
    });
    this.userService.fetchParticular(this.id);
    this.userService.particularFetcher.subscribe((data: object) => {
      this.detailedUser = data;
    });
  }

  ngOnDestroy() {
    this.userService.particularFetcher.unsubscribe();
  }

}
